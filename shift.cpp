#include <iostream>

using namespace std;

void shift(char arr[], int step, bool);
int getLength(char arr[]);
void displayArray(char arr[]);

int main()
{
    char arr[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', '\0'};
    shift(arr, 3, 0);
    displayArray(arr);
}

int getLength(char arr[])
{
    int i = 0;
    while (arr[i] != 0)
    {
        i++;
    }
    return i;
}

void displayArray(char arr[])
{
    for (size_t i = 0; arr[i]; i++)
    {
        cout << arr[i] << " ";
    }
}

void shift(char arr[], int step, bool isRightDirection)
{
    int arrLength = getLength(arr);

    for (size_t i = 0; i < step; i++)
    {
        if (isRightDirection)
        {
            for (size_t j = arrLength - 1; j > 0; j--)
            {
                swap(arr[j], arr[j - 1]);
            }
        }
        else
        {
            for (size_t j = 1; j < arrLength; j++)
            {
                swap(arr[j], arr[j - 1]);
            }
        }
    }
}

void swap(char& a, char& b)
{
    char tmp = a;
    a = b;
    b = tmp;
}